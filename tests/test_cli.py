#!/usr/bin/env python3

# Copyright © 2023 Red Hat, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import mr_label_maker.mr_label_maker as mlm

from pathlib import Path
from unittest.mock import patch, MagicMock
import datetime as dt
import pytest


@pytest.fixture
def token() -> str:
    return "topsecret"


@pytest.fixture
def yamlfile(tmpdir) -> Path:
    name = Path(tmpdir) / "mr-label-maker-test.yml"
    with open(name, "w") as fd:
        fd.write(
            """
            version: 1
            project: { name: 'test', id: 123 }
        """
        )
    return name


@pytest.fixture
def default_config(yamlfile) -> mlm.ProjectConfig:
    with open(yamlfile) as fd:
        return mlm.ProjectConfig.from_yaml(fd.read())


@pytest.fixture
def config_args(yamlfile: Path) -> list[str]:
    return ["--config", str(yamlfile.absolute())]


@pytest.fixture
def default_args(config_args, token) -> list[str]:
    return config_args + ["--token", token]


# Note: patch order is reverse of the test function argument order
@patch("mr_label_maker.mr_label_maker.GitLabProject")
@patch("gitlab.gitlab.Gitlab")  # So we can't ever instantiate the real one
class TestCli:
    def test_missing_config(self, gitlab, project):
        exit_code = mlm.main(["--issues", "--config", "does-not-exist.yml"])
        assert exit_code == mlm.ExitCode.BUG

    def test_invalid_project(self, gitlab, project):
        with pytest.raises(SystemExit) as excinfo:
            mlm.main(["--issues", "--project", "foobar"])
        # argparse calls sys.exit(2) for
        # invalid arguments. That's the same as our API_ERROR
        assert excinfo.value.code == mlm.ExitCode.API_ERROR

    def test_missing_token(self, gitlab, project, monkeypatch, config_args):
        monkeypatch.setenv("XDG_CONFIG_HOME", "/does-not-exist")
        exit_code = mlm.main(["--issues", *config_args])
        assert exit_code == mlm.ExitCode.USER_ERROR

        exit_code = mlm.main(["--issues", *config_args, "--token", ""])
        assert exit_code == mlm.ExitCode.USER_ERROR

    @pytest.mark.parametrize("create_labels", (True, False))
    def test_check_labels(self, gitlab, project, token, tmpdir, capsys, create_labels):
        proj = MagicMock()
        project.return_value = proj  # override constructor
        proj.fetch_labels.return_value = ["barlabel", "bazlabel"]

        name = Path(tmpdir) / "mr-label-maker-test.yml"
        with open(name, "w") as fd:
            fd.write(
                """
                version: 1
                project: { name: 'test', id: 123 }
                issues:
                    topics:
                     'a': 'foolabel'
                     'b': 'barlabel'
            """
            )

        args = ["--issues", "--token", token, "--config", str(name.absolute())]
        if create_labels:
            args.append("--create-labels")
        exit_code = mlm.main(args)
        if create_labels:
            assert exit_code == mlm.ExitCode.SUCCESS
        else:
            assert exit_code == mlm.ExitCode.USER_ERROR
            out, err = capsys.readouterr()
            assert "Labels that do not exist in the project: 'foolabel'" in out

    def test_toml_token(self, gitlab, project, monkeypatch, tmpdir, config_args):
        pytest.importorskip("tomllib")
        proj = MagicMock()
        project.return_value = proj  # override constructor

        monkeypatch.setenv("XDG_CONFIG_HOME", str(Path(tmpdir)))
        tomlfile = Path(tmpdir) / "mr-label-maker" / "tokens.toml"
        tomlfile.parent.mkdir(parents=True)
        with open(tomlfile, "w") as fd:
            fd.write("['https://gitlab.example.com']\n")
            fd.write("token = 'tokenval'\n")
            fd.flush()

        configfile = Path(tmpdir) / "test-config.yml"
        with open(configfile, "w+") as fd:
            fd.write("version: 1\n")
            fd.write(
                "project: { name: 'projectname', id: 123, instance: 'https://gitlab.example.com' }"
            )
            fd.flush()

        args = ["--config", str(configfile.absolute()), "--issues"]
        exit_code = mlm.main(args)
        assert exit_code == mlm.ExitCode.SUCCESS
        proj.connect.assert_called()
        kall = project.call_args
        assert kall.kwargs["config"].name == "projectname"
        assert kall.kwargs["token"] == "tokenval"

    @pytest.mark.parametrize("dry_run", (True, False))
    @pytest.mark.parametrize("label", (True, False))
    @pytest.mark.parametrize("state", [None, "opened", "closed", "merged", "all"])
    @pytest.mark.parametrize("ignore_history", (True, False))
    def test_pass_args(
        self,
        gitlab,
        project,
        token,
        config_args,
        default_config,
        dry_run,
        label,
        state,
        ignore_history,
    ):
        proj = MagicMock()
        project.return_value = proj  # override constructor

        # Fixed args because otherwise we don't get anywhere
        args = [
            *config_args,
            "--issues",
        ]
        if dry_run:
            args.append("--dry-run")

            # for dry-run, we don't use --token
            token = None
        else:
            args.extend(["--token", token])

        if label:
            args.extend(["--label", "foolabel"])
        if state:
            args.extend(["--state", state])
        if ignore_history:
            args.append("--ignore-label-history")

        exit_code = mlm.main(args)
        assert exit_code == mlm.ExitCode.SUCCESS

        # Not let's make sure we passed everything through as expected
        project.assert_called_with(
            config=default_config,
            token=token,
            dry_run=dry_run,
            label=["foolabel"] if label else [],
            state=(state or "opened"),
            ignore_label_history=ignore_history,
        )
        proj.connect.assert_called()

    @pytest.mark.parametrize("arg_name", ["--issues", "-i"])
    @pytest.mark.parametrize("arg", ["skip", "specific", "no-arg"])
    @pytest.mark.parametrize("since", [None, "60"])
    @patch("datetime.datetime")
    def test_pass_issues(
        self, datetime, gitlab, project, default_args, token, arg_name, arg, since
    ):
        proj = MagicMock()
        project.return_value = proj  # override constructor

        now = dt.datetime.now(dt.timezone.utc)
        datetime.now.return_value = now

        args = [*default_args]

        if arg == "skip":
            pass
        elif arg == "specific":
            args += [arg_name, "123"]
        elif arg == "no-arg":
            args += [arg_name]

        if since:
            args += ["--since", since]

        exit_code = mlm.main(args)
        if arg == "skip":
            assert exit_code == mlm.ExitCode.USER_ERROR
        else:
            assert exit_code == mlm.ExitCode.SUCCESS
            proj.connect.assert_called()

        expected_args = {}
        if since:
            since = now - datetime.timedelta(minutes=int(since))
            expected_args["since"] = since
        else:
            expected_args["since"] = None

        if arg == "skip":
            proj.process_issues.assert_not_called()
        else:
            if arg == "specific":
                expected_args["issue_id"] = 123
            elif arg == "no-arg":
                expected_args["issue_id"] = -1
            proj.process_issues.assert_called_with(**expected_args)

    @pytest.mark.parametrize("arg_name", ["--merge-requests", "-m", "--mrs"])
    @pytest.mark.parametrize("arg", ["skip", "specific", "no-arg"])
    @pytest.mark.parametrize("since", [None, "60"])
    @patch("datetime.datetime")
    def test_pass_merge_request(
        self, datetime, gitlab, project, default_args, arg_name, arg, since
    ):
        proj = MagicMock()
        project.return_value = proj  # override constructor

        now = dt.datetime.now(dt.timezone.utc)
        datetime.now.return_value = now

        args = [*default_args]

        if arg == "skip":
            pass
        elif arg == "specific":
            args += [arg_name, "123"]
        elif arg == "no-arg":
            args += [arg_name]

        if since:
            args += ["--since", since]

        exit_code = mlm.main(args)
        if arg == "skip":
            assert exit_code == mlm.ExitCode.USER_ERROR
        else:
            assert exit_code == mlm.ExitCode.SUCCESS
            proj.connect.assert_called()

        expected_args = {}
        if since:
            since = now - datetime.timedelta(minutes=int(since))
            expected_args["since"] = since
        else:
            expected_args["since"] = None

        if arg == "skip":
            proj.process_mrs.assert_not_called()
        else:
            if arg == "specific":
                expected_args["mr_id"] = 123
            elif arg == "no-arg":
                expected_args["mr_id"] = -1
            proj.process_mrs.assert_called_with(**expected_args)

    @pytest.mark.parametrize(
        "config",
        [
            None,
            "./.mr-label-maker.yml",
            ".gitlab-ci/mr-label-maker.yml",
            "foo.yaml",
            "bar/bar.txt",
        ],
    )
    def test_config_file(self, gitlab, project, monkeypatch, token, tmpdir, config):
        monkeypatch.chdir(tmpdir)
        if config is not None:
            Path(config).parent.mkdir(exist_ok=True)
            print(f"creating   {Path(config).resolve()}")
            with open(config, "w+") as fd:
                fd.write("version: 1\n")
                fd.write("project: { name: 'test', id: 123 }")
                fd.flush()

        proj = MagicMock()
        project.return_value = proj  # override constructor

        args = [
            "--token",
            token,
            "--issues",
        ]
        if config and "mr-label-maker" not in config:
            args.extend(["--config", config])

        exit_code = mlm.main(args)
        if config is None:
            assert exit_code == mlm.ExitCode.USER_ERROR
        else:
            assert exit_code == mlm.ExitCode.SUCCESS
            _, _, kwargs = project.mock_calls[0]
            c = kwargs["config"]
            assert c.gitlab_id == 123

    @pytest.mark.parametrize(
        "url",
        [
            "https://example.com/filename.yml",
            "http://example.org/filename.txt",
            "https://needs-a-retry",
            "https://does-not-exist",
        ],
    )
    @patch("urllib.request.urlopen")
    @patch("time.sleep")
    def test_config_file_url(self, sleep, urlopen, gitlab, project, token, url):
        import urllib.error

        error = urllib.error.HTTPError(url, 404, "boom", MagicMock(), MagicMock())

        response = MagicMock()
        response.__enter__.return_value = response
        response.read.return_value = """
            version: 1
            project: { name: 'test', id: 123 }
        """

        if "needs-a-retry" in url:
            urlopen.side_effect = [error, response]
        if "does-not-exist" in url:
            urlopen.side_effect = error
        else:
            urlopen.return_value = response

        args = ["--token", token, "--issues", "--config", url]

        exit_code = mlm.main(args)
        if "does-not-exist" in url:
            assert exit_code == mlm.ExitCode.USER_ERROR
        else:
            assert exit_code == mlm.ExitCode.SUCCESS
            urlopen.assert_called_with(url)
            _, _, kwargs = project.mock_calls[0]
            c = kwargs["config"]
            assert c.gitlab_id == 123
